@isTest
private class ContractDetailPageControllerTest { 
    static testMethod void myUnitTest() {
    		 Account Accnt = new Account(Name='Test FName');
			 insert Accnt;
			 Contract con = new Contract(AccountId=Accnt.Id,Status='Draft',ContractTerm = 12);
             Clauses__c clause = new Clauses__c(Name='test clause',Type__c='Other');
             insert con;
             insert clause;
             Contract_to_Clause__c ctc = new Contract_to_Clause__c();
             ctc.Name = ' test Contract to Clause';
             ctc.Contract__c = con.Id;
     		 ctc.Clauses__c = clause.Id;
             upsert ctc;
             ContractDetailPageController cDpc = new ContractDetailPageController();
    		 ApexPages.StandardController sc = new ApexPages.StandardController(con);
        	 ContractDetailPageController cdpObj = new ContractDetailPageController(sc);
    		test.startTest();
    	 		cdpObj.getContractList();
        		cdpObj.getContractToClauseList();
                cdpObj.getClauseList();
        		cdpObj.getcontract();
                cdpObj.cancel();
        		cdpObj.newrecords();
        	 PageReference testPage = new pagereference('/apex/ContractDetailPage');
             ApexPages.currentPage().getParameters().put( 'cId2', ctc.id );
        		cdpObj.deleteC();
        
        	test.stopTest();
    }
       
}