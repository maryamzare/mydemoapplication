/***********************************************
* This class Shows the details of the Contract bject(List) then has a new button to 
* direct to creating a new contrac
* the Contract object has a related list (Contract_to_Clause__c object) which shows the
* Contract_to_Clause__c 's records. Contract_to_Clause__c has a new button which direct you to 
* the Clauses__c object (include diffrent clause), it has a New button which open a window that has contract 
* ID . The window has three input form :Name, contract ( pickList), clause(pickList) 
*/
 public class ContractDetailPageController {
   
    public id currentPageID{get;set;}
    public list<Contract_to_Clause__c> contractToClauseList{get;set;} 
    public List<Clauses__c> clauseList{get;set;} 
    public List<Contract> contractList{get;set;}
     
    private final Contract contract{get;set;}
           
    public ContractDetailPageController(ApexPages.StandardController controller){ 
        
             this.currentPageID = ApexPages.currentPage().getParameters().get('id'); 
             this.contract = (Contract)Controller.getRecord();
                           
             contractList=[SELECT ContractNumber , AccountId, Status ,StartDate ,EndDate ,ContractTerm  
                           FROM Contract];
             contractToClauseList = [SELECT Name, Id__c  
                                     FROM Contract_to_Clause__c 
                                     WHERE Contract__c  = :ApexPages.currentPage().getParameters().get('id')];
     }

   public ContractDetailPageController() {
                    
    }
/***********************************************
*This methods get ObjectsList 
*/
    public List<Contract> getContractList() {                        
           return contractList;
      }
     
    public List<Contract_to_Clause__c> getContractToClauseList() {                             
           return contractToClauseList;
     }

    public List<Clauses__c> getClauseList() {                  
          return clauseList;
     } 
/***********************************************
*This methods get Objects 
*/
     public contract getcontract(){
        return contract;
     }
 
/***********************************************
*This method add newrecords for contracts
*/
    public pageReference newrecords(){
        Pagereference pref= 
        new Pagereference('/apex/NewContractToClause?refRecord='+currentPageID);
        pref.setRedirect(true);
        return pref;
     }
    
 /***********************************************
*This method delete from Contract to clause list
*/ 
     public Pagereference deleteC(){
 
         String cId= ApexPages.currentPage().getParameters().get('cId2'); 
         system.debug('the value of cid is:' +cId);
     
         Contract_to_Clause__c del = [Select Name, id from Contract_to_Clause__c where id =:cId limit 1];
            if(del !=null ){
            delete del;
            }
     
         PageReference nextPage = new PageReference('/apex/ContractDetailPage?id='+currentPageID); 
         return nextPage.setRedirect(true); 
 }   
 /***********************************************
*This method is for cancel 
*/         

      public PageReference cancel() {
            return null;
      }
 }
/*End of the Class*/