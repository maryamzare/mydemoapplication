@isTest
public class NewContractToClauseTest {
 static testMethod void myUnitTest() {
        test.startTest();
             Account Accnt = new Account(Name='Test FName');
             insert Accnt;
             Contract con = new Contract(AccountId=Accnt.Id,Status='Draft',ContractTerm = 12);
             Clauses__c clause = new Clauses__c(Name='test clause',Type__c='Other');
             insert con;
             insert clause;
             Contract_to_Clause__c ctc = new Contract_to_Clause__c(Contract__c = con.Id, Clauses__c = clause.Id);
             ctc.Name = ' test Contract to Clause';
             ctc.Contract__c = con.Id;
             ctc.Clauses__c = clause.Id;
             upsert ctc;
             PageReference testPage = new pagereference('/apex/NewContractToClause');
             ApexPages.currentPage().getParameters().put( 'id', ctc.id );
             NewContractToClause ctcObj = new NewContractToClause();
             ctcObj.cds = ctc;
             PageReference pageref = ctcObj.save();
             ctcObj.getContractToClause();
             ctcObj.Cancel();
        test.stopTest();
 }
}